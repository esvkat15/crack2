#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "md5.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    
    // Hash the guess using MD5
    char* guesh = md5(guess, strlen(guess));
    // Compare the two hashes
    int r = !strncmp(guesh, hash, HASH_LEN);
    // Free any malloc'd memory
    free(guesh);
    return r;
    
}

// Read in a file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **readfile(char *filename)
{
    
    struct stat filesize;
    if(stat(filename, &filesize) == -1) {
        
        fprintf(stderr, "Can't get info about %s\n", filename);
        exit(1);
        
    }
    int size = filesize.st_size;
    char* buffer = malloc((size + 1) * sizeof(char));
    FILE* fp = fopen(filename, "r");
    if(!fp) {
        
        fprintf(stderr, "Can't open %s\n", filename);
        exit(1);
        
    }
    fread(buffer, 1, size, fp);
    fclose(fp);
    buffer[size] = '\0';
    int i = 1;
    char** spointers = malloc(sizeof(char*));
    *spointers = buffer;
    do if(*buffer == '\n') {
        
        *buffer = '\0';
        spointers = realloc(spointers, (i + 1) * sizeof(char*));
        spointers[i++] = buffer + 1;
        
    } while(*(++buffer));
    spointers = realloc(spointers, (i + 1) * sizeof(char*));
    spointers[i] = NULL;
    return spointers;
    
}


int main(int argc, char *argv[])
{
    
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the hash file into an array of strings
    char **hashes = readfile(argv[1]);

    // Read the dictionary file into an array of strings
    char **dict = readfile(argv[2]);
    
    
    
    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    int i = 0, j = 0;
    while(hashes[j]) {
        
        while(dict[i])
            if(tryguess(hashes[j], dict[i++]))
                printf("hash: %s\npassword: %s\n", hashes[j], dict[i]);
        i = 0;
        j++;
    }
    free(hashes[0]);
    free(hashes);
    free(dict[0]);
    free(dict);
    
}